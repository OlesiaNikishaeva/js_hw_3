console.log("----------------------------------------------------------------")

// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers"

let userNumber = +prompt('Enter a number');

while (Number.isInteger(userNumber) === false) {
  userNumber = +prompt('Please enter a correct number');
}

if (userNumber === 5) {
  console.log(userNumber)
} else if (userNumber < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 5; i <= userNumber; i = i + 5) {
    console.log(i);
  }
}



console.log("----------------------------------------------------------------")

// Отримати два числа, m і n. Вивести в консоль усі прості числа в діапазоні від m до n (менше із введених чисел буде m, більше буде n). Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище, вивести повідомлення про помилку і запитати обидва числа знову.

let m = +prompt("Enter a first number");
let n = +prompt("Enter a second number");

while (
  m > n ||
  Number.isInteger(m) === false ||
  Number.isInteger(n) === false
) {
  m = +prompt("Enter a first number again");
  n = +prompt("Enter a second number again");
}

for (let i = m; i <= n; i++) {
  console.log("i", i);
  if (i === 1 || i === 2) {
    console.log("i === 2");
    console.log("prime", i);
  } else if (i % 2 !== 0 && i !== 1) {
    let primeNumb = true;

    for (let j = 2; j < i; j++) {
      console.log("j", j);
      if (i % j === 0) {
        console.log("i % j === 0");
        primeNumb = false;
        break;
      }
    }

    if (primeNumb) {
      console.log("prime", i);
    }
  }
}